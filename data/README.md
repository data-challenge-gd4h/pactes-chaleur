# Source de données

| Données | Accès | Source | Lien | Période traitée |
| ------- | ----- | ------ | ---- | --------------- |
| **Indice de défavorisation sociale (FDep) par IRIS** | Par commune | Open Data Soft | https://www.cepidc.inserm.fr/documentation/indicateurs-ecologiques-du-niveau-socio-economique | 2015 |
| **Température (°C)** | Par commune | Historical Weather API, qui se base sur Copernicus | https://open-meteo.com/en/docs/historical-weather-api | Eté 2022 (juin, juillet, août) |
| **Equipement des résidences principales en climatisation** | Par commune | Base de données nationale des bâtiments (BDNB), disponible sur le site data.gouv.fr | https://www.data.gouv.fr/fr/datasets/base-de-donnees-nationale-des-batiments/ | 2022 |
| **Couverture végétale (CORINE Land Cover)** | Par parcelle | data.gouv.fr | https://www.data.gouv.fr/fr/datasets/corine-land-cover-occupation-des-sols-en-france/ | 2018 |
| **Découpage administratif communal français** (pour les cartes) | Par commune | data.gouv.fr | https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/ | 2022 | 

# Référentiel des communes (2022)

Nous utilisons le référentiel de 2022 fourni par l'INSEE, qu'on peut télécharger sur [sirse.atlasante.fr](https://sirse.atlasante.fr/#c=externaldata).

Dans le cadre de ce challenge, on s'intéresse seulement à la France métropolitaine. Ainsi, nous avons : 
- Communes : 34 955
- Communes et arrondissements : 34 997
- Communes et arrondissements sans les DOM TOM : 34 868

# Fichiers finaux (data/processed)

| Fichier | Colonnes | Script |
| ------- | -------- | ------ |
| comm_arr_hors_dom_2022_all_indicators.csv | code_commune_insee,libelle_commune_insee,fdep15,batiments_avec_climatisation_pourcentage,temperature_moyenne_ete_C,tx_cv,z_score_fdep15,z_score_batiments_avec_climatisation_pourcentage,z_score_temperature_moyenne_ete_C,z_score_tx_cv | [pactes_chaleur/indicators/compute_all_indicators.py](../pactes_chaleur/indicators/compute_all_indicators.py) |
|comm_arr_hors_dom_2022_batiments_avec_climatisation_pourcentage.csv | code_commune_insee,libelle_commune_insee,total_batiments,batiments_sans_climatisation,batiments_avec_climatisation,batiments_avec_climatisation_pourcentage | [pactes_chaleur/indicators/air_conditioning/compute_air_conditioning.py](../pactes_chaleur/indicators/air_conditioning/compute_air_conditioning.py)| 
| comm_arr_hors_dom_2022_fdep15.csv |code_commune_insee,libelle_commune_insee,fdep15| [pactes_chaleur/indicators/deprivation_index/compute_deprivation_index.py](../pactes_chaleur/indicators/deprivation_index/compute_deprivation_index.py) |
| comm_arr_hors_dom_2022_indic_vuln.csv | code_commune_insee,libelle_commune_insee,indic_vuln | [pactes_chaleur/heat_score/compute_heat_score.py](../pactes_chaleur/heat_score/compute_heat_score.py) |
| comm_arr_hors_dom_2022_temperature_moyenne_ete_C.csv | lcode_commune_insee,ibelle_commune_insee,latitude,longitude,temperature_moyenne_ete_C | [pactes_chaleur/indicators/temperature/compute_temperature.py](../pactes_chaleur/indicators/temperature/compute_temperature.py) |
| comm_arr_hors_dom_2022_tx_cv.csv | code_commune_insee,tx_cv | Pas de script, utilisation de SIS QGis |
| PACTES_chaleur_atlasante.csv | code_commune_insee,libelle_commune_insee,indic_vuln,nb_iCanicule,nb_hospits_iCanicule | [pactes_chaleur/atlasante/create_file_for_atlasante.py](../pactes_chaleur/atlasante/create_file_for_atlasante.py) |

