"""
compute_morbidity.py
Main script to get the number of emergency room visits and emergency room visits followed by hospitalization
for each metropolitan French town of the 2022 INSEE referential.
"""

import pandas as pd
import numpy as np


def add_morbidity():
    """Compute the number of emergency room visits and emergency room visits followed by hospitalization
    for each metropolitan French town of the 2022 INSEE referential."""

    morbidity_df = pd.read_csv(
        "./data/raw/morbidity/nb_IC_AGG_Insee.csv", sep=";", encoding="latin1"
    )
    morbidity_df = (
        morbidity_df.rename(columns={"code_insee": "code_commune_insee"})
        .dropna(axis=0, how="all", subset=["code_commune_insee"])
        .assign(code_commune_insee=lambda df: df["code_commune_insee"].astype(str).str.zfill(5))
        .replace("nan", np.nan)
        .groupby("code_commune_insee")
        .agg({"nb_iCanicule": "sum", "nb_hospits_iCanicule": "sum"})
        .reset_index()
        .assign(
            nb_iCanicule=lambda df: df["nb_iCanicule"].astype(str).str.rstrip(".0"),
            nb_hospits_iCanicule=lambda df: df["nb_hospits_iCanicule"].astype(str).str.rstrip(".0"),
        )
    )
    morbidity_df.to_csv("./data/intermediate/morbidity/morbidity.csv", index=False)


if __name__ == "__main__":
    add_morbidity()
