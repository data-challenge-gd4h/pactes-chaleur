# Préparation des données de morbidité

## 1. Récupération des données 

Les données ont été récupérées auprès de Santé Publique France. Le fichier contient 6205 lignes et 6 colonnes. 

Les variables contiennent les informations suivantes : 
- rge_code_etablissement : code unique du service d'urgence
- annee_entree : année de l'entrée du passage aux urgences
- nb_iCanicule : nombre de passages aux urgences pour l'indicateur chaleur
- nb_hospits_iCanicule : nombre de passages aux urgences suivi d'une hospitalisation pour l'indicateur chaleur
- Libelle : libellé de l'établissement
- code_insee : code insee de la commune de l'établissement

## 2. Traitements des données

La somme des passages aux urgences et passages aux urgences suivis d'une hospitalisation a été calculée par commune sur la période d'étude de 2012 à 2022. 
