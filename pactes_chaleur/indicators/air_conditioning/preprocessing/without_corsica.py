import pandas as pd
from typing import List


from pactes_chaleur.indicators.air_conditioning.preprocessing.utils import (
    compute_buildings_with_and_without_air_conditioning,
    fill_with_mean,
    compute_mean_values_based_on_close_temperature,
    compute_total_buildings,
    compute_buildings_with_ac_percent,
)


def remove_corsica(df: pd.DataFrame) -> pd.DataFrame:
    """Remove Corsica from code_commune_insee"""

    return (
        df.drop(df[df["code_commune_insee"].astype(str).str.contains("A|B")].index)
        .dropna(subset=["code_commune_insee"])
        .astype({"code_commune_insee": "float"})
        .astype({"code_commune_insee": "int"})
    )


def merge_data(
    df1: pd.DataFrame, df2: pd.DataFrame, df2_columns: List[str], on: str, how: str
) -> pd.DataFrame:
    df2[on] = df2[on].astype("int64")
    return pd.merge(df1, df2[df2_columns], on=on, how=how)


def compute_means(df: pd.DataFrame, columns: List[str], on: str) -> pd.DataFrame:
    """Compute mean buildings with and without air conditioning for each group 'CAN'"""

    means_df = df.groupby(on)[columns].mean().reset_index()
    means_df.columns = [on, "moy_batiments_avec_climatisation", "moy_batiments_sans_climatisation"]
    return pd.merge(df, means_df, on=on, how="left")


def add_arrondissements_ac(
    df_to_update: pd.DataFrame, df_to_pull_from: pd.DataFrame, code_range: range
):
    """Updating dataframe with Paris, Lyon and Marseille arrondissements
    For each commune code, get the corresponding values for buildings with
    and without air conditioning from the df_to_pull_from DataFrame
    and update those values in the df_to_update DataFrame.
    """

    for code in code_range:
        with_air_conditioning = df_to_pull_from.loc[
            df_to_pull_from["code_commune_insee"] == code, "batiments_avec_climatisation"
        ].values[0]
        without_air_conditioning = df_to_pull_from.loc[
            df_to_pull_from["code_commune_insee"] == code, "batiments_sans_climatisation"
        ].values[0]

        df_to_update.loc[
            df_to_update["code_commune_insee"] == code, "batiments_avec_climatisation"
        ] = with_air_conditioning
        df_to_update.loc[
            df_to_update["code_commune_insee"] == code, "batiments_sans_climatisation"
        ] = without_air_conditioning

    return df_to_update


def add_air_conditioning_without_corsica():
    """Process air conditioning data of metropolitan France without corsica."""

    # Residential buildings data
    buildings_df = pd.read_csv(
        "./data/intermediate/air_conditioning/residential_buildings_2022.csv", sep=","
    )
    buildings_df = remove_corsica(buildings_df)
    buildings_ac_df = compute_buildings_with_and_without_air_conditioning(buildings_df)

    # Cantons
    cantons_df = pd.read_csv("./data/raw/comm_can_2022.csv", sep=";")
    cantons_df = remove_corsica(cantons_df)
    cantons_subset_df = cantons_df[["code_commune_insee", "CAN", "LIBELLE", "DEP"]]

    # Merge buildings AC and cantons dataframes
    buildings_ac_cantons_df = merge_data(
        buildings_ac_df,
        cantons_subset_df,
        ["code_commune_insee", "CAN", "LIBELLE", "DEP"],
        "code_commune_insee",
        "left",
    )
    buildings_ac_cantons_df.drop_duplicates(subset="code_commune_insee", keep="first", inplace=True)
    buildings_ac_cantons_df = compute_means(
        buildings_ac_cantons_df,
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
        "CAN",
    )

    # INSEE towns data
    towns_df = pd.read_csv("./data/intermediate/comm_arr_hors_dom_2022.csv")
    towns_df = remove_corsica(towns_df)

    # Merge buildings AC, cantons and communes dataframes
    buildings_ac_cantons_towns_df = merge_data(
        buildings_ac_cantons_df, towns_df, ["code_commune_insee"], "code_commune_insee", "outer"
    )
    buildings_ac_cantons_towns_df = merge_data(
        buildings_ac_cantons_towns_df,
        cantons_subset_df,
        ["code_commune_insee", "CAN", "LIBELLE", "DEP"],
        "code_commune_insee",
        "left",
    )
    buildings_ac_cantons_towns_df = buildings_ac_cantons_towns_df.drop(
        ["CAN_x", "LIBELLE_x", "DEP_x"], axis=1
    ).rename(columns={"CAN_y": "CAN", "LIBELLE_y": "Libelle", "DEP_y": "Dep"})

    # Fill missing value with mean of cantan
    buildings_ac_cantons_towns_df = fill_with_mean(
        buildings_ac_cantons_towns_df,
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
        "CAN",
    )

    # Temperature data
    temperature_df = pd.read_csv(
        "./data/processed/comm_arr_hors_dom_2022_temperature_moyenne_ete_C.csv"
    )
    temperature_df = remove_corsica(temperature_df)
    buildings_ac_cantons_towns_temp_df = merge_data(
        buildings_ac_cantons_towns_df,
        temperature_df[["code_commune_insee", "temperature_moyenne_ete_C"]],
        ["code_commune_insee", "temperature_moyenne_ete_C"],
        "code_commune_insee",
        "left",
    )

    # Add Paris, Lyon, and Marseille arrondissements
    lyon_df = pd.read_csv("./data/intermediate/air_conditioning/lyon.csv", sep=";")
    paris_df = pd.read_csv("./data/intermediate/air_conditioning/paris.csv", sep=";")
    marseille_df = pd.read_csv("./data/intermediate/air_conditioning/marseille.csv", sep=";")
    buildings_ac_cantons_towns_temp_df = add_arrondissements_ac(
        buildings_ac_cantons_towns_temp_df, paris_df, range(75101, 75121)
    )
    buildings_ac_cantons_towns_temp_df = add_arrondissements_ac(
        buildings_ac_cantons_towns_temp_df, lyon_df, range(69381, 69390)
    )
    buildings_ac_cantons_towns_temp_df = add_arrondissements_ac(
        buildings_ac_cantons_towns_temp_df, marseille_df, range(13201, 13217)
    )
    buildings_ac_cantons_towns_temp_df = buildings_ac_cantons_towns_temp_df.drop_duplicates(
        subset="code_commune_insee", keep="first"
    )

    final_df = compute_mean_values_based_on_close_temperature(
        buildings_ac_cantons_towns_temp_df,
        "temperature_moyenne_ete_C",
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
    )
    final_df = compute_total_buildings(
        final_df,
        "total_batiments",
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
    )
    final_df = compute_buildings_with_ac_percent(
        final_df,
        "batiments_avec_climatisation_pourcentage",
        "batiments_avec_climatisation",
        "total_batiments",
    )

    final_df = final_df.astype({"code_commune_insee": str}).assign(
        code_commune_insee=lambda df: df["code_commune_insee"].str.zfill(5)
    )

    return final_df


if __name__ == "__main__":
    add_air_conditioning_without_corsica()
