"""
utils.py
Functions to import in corsica.py and without_corsica.py
"""

import pandas as pd
from typing import List


def compute_buildings_with_and_without_air_conditioning(buildings_df: pd.DataFrame) -> pd.DataFrame:
    """Compute buildings with and without air conditioning per town"""

    return (
        buildings_df.groupby("code_commune_insee")
        .agg({"presence_climatisation": ["count", "sum"]})
        .rename(columns={"count": "total_batiments", "sum": "batiments_avec_climatisation"})
        .droplevel(0, axis=1)
        .assign(
            batiments_sans_climatisation=lambda df: df["total_batiments"]
            - df["batiments_avec_climatisation"],
            batiments_avec_climatisation_pourcentage=lambda df: df["batiments_avec_climatisation"]
            / df["total_batiments"]
            * 100,
        )
        .round({"batiments_avec_climatisation_pourcentage": 2})
        .reset_index()
    )


def fill_with_mean(df: pd.DataFrame, columns: List[str], on: str) -> pd.DataFrame:
    """Fill missing values with mean values based on 'CAN'"""

    columns_subset = columns + [on]
    grouped_df = df[columns_subset].groupby(on).mean()
    for canton in grouped_df.index:
        mean_values = grouped_df.loc[canton, columns]
        cantons_towns = df[df[on] == canton].index
        for col in columns:
            for town in cantons_towns:
                if pd.isnull(df.loc[town, col]):
                    if col == "batiments_avec_climatisation" and mean_values[col] < 1:
                        df.loc[town, col] = 0
                    else:
                        df.loc[town, col] = mean_values[col]
    return df


def compute_mean_values_based_on_close_temperature(
    df: pd.DataFrame, group_column: str, value_columns: List[str]
) -> pd.DataFrame:
    """Function to fill missing values with the mean of group 'temperature_moyenne_ete_C'"""

    for column in value_columns:
        mean_values = df.groupby(group_column)[column].transform("mean")
        df[column].fillna(mean_values, inplace=True)
    return df


def compute_total_buildings(
    df: pd.DataFrame, total_buildings_column: str, columns: List[str]
) -> pd.DataFrame:
    """Function to compute total buildings.
    If total buildings is missing, compute it as sum of buildings with and without air conditioning
    """

    df[total_buildings_column] = df.apply(
        lambda row: sum(row[col] for col in columns)
        if pd.isnull(row[total_buildings_column])
        else row[total_buildings_column],
        axis=1,
    )
    return df


def compute_buildings_with_ac_percent(
    df: pd.DataFrame, percentage_column: str, nominator_column: str, denominator_column: str
) -> pd.DataFrame:
    """Function to compute percentage of buildings with air conditioning
    If the percentage is missing, compute it as (buildings with air conditioning / total buildings) * 100
    """

    df[percentage_column] = df.apply(
        lambda row: "{:.2f}".format((row[nominator_column] / row[denominator_column]) * 100)
        if pd.isnull(row[percentage_column])
        else row[percentage_column],
        axis=1,
    )
    return df
