import pandas as pd

from pactes_chaleur.indicators.air_conditioning.preprocessing.utils import (
    compute_buildings_with_and_without_air_conditioning,
    fill_with_mean,
    compute_mean_values_based_on_close_temperature,
    compute_total_buildings,
    compute_buildings_with_ac_percent,
)


def merge_with_cantons(df: pd.DataFrame, cantons_df: pd.DataFrame) -> pd.DataFrame:
    """Merge dataframe with cantons data."""

    cantons_subset_df = cantons_df[["code_commune_insee", "CAN", "LIBELLE"]]
    cantons_subset_df = cantons_subset_df[
        cantons_subset_df["code_commune_insee"].astype(str).str.contains("A|B")
    ]
    return pd.merge(
        df, cantons_subset_df[["code_commune_insee", "CAN"]], on="code_commune_insee", how="left"
    )


def merge_with_towns_insee(df: pd.DataFrame, towns_df: pd.DataFrame) -> pd.DataFrame:
    """Merge dataframe with towns data."""

    return (
        df.merge(towns_df, on="code_commune_insee", how="outer")
        .drop(["CAN_x"], axis=1)
        .rename(columns={"CAN_y": "CAN"})
        .drop_duplicates(subset=["code_commune_insee"], keep="first")
    )


def merge_with_temperature(df: pd.DataFrame, temperature_df: pd.DataFrame) -> pd.DataFrame:
    """Merge dataframe with temperature data."""

    return pd.merge(
        df,
        temperature_df[["code_commune_insee", "temperature_moyenne_ete_C"]],
        on="code_commune_insee",
        how="left",
    )


def add_air_conditioning_for_corsica():
    """Process air conditioning data of Corsica."""

    # Load data
    buildings_df = pd.read_csv(
        "./data/intermediate/air_conditioning/residential_buildings_2022.csv", sep=","
    )
    cantons_df = pd.read_csv("./data/raw/comm_can_2022.csv", sep=";")
    corsica_df = pd.read_csv("./data/intermediate/air_conditioning/comm_corsica_2022.csv", sep=";")
    temperature_df = pd.read_csv(
        "./data/processed/comm_arr_hors_dom_2022_temperature_moyenne_ete_C.csv"
    )

    # Clean and process data
    corsica_buildings_df = buildings_df[
        buildings_df["code_commune_insee"].astype(str).str.contains("A|B")
    ]
    corsica_buildings_ac_df = compute_buildings_with_and_without_air_conditioning(
        corsica_buildings_df
    )
    corsica_buildings_ac_cantons_df = merge_with_cantons(corsica_buildings_ac_df, cantons_df)
    corsica_buildings_ac_cantons_towns_df = merge_with_towns_insee(
        corsica_buildings_ac_cantons_df, corsica_df
    )
    corsica_buildings_ac_cantons_towns_df = fill_with_mean(
        corsica_buildings_ac_cantons_towns_df,
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
        "CAN",
    )
    corsica_buildings_ac_cantons_towns_temp_df = merge_with_temperature(
        corsica_buildings_ac_cantons_towns_df, temperature_df
    )
    corsica_buildings_ac_cantons_towns_temp_df = compute_mean_values_based_on_close_temperature(
        corsica_buildings_ac_cantons_towns_temp_df,
        "temperature_moyenne_ete_C",
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
    )

    # Specific case for '2B14'
    if "2B14" in corsica_buildings_ac_cantons_towns_temp_df["CAN"].values:
        # Get the values from rows with '2B14'
        values_2B14 = corsica_buildings_ac_cantons_towns_temp_df.loc[
            corsica_buildings_ac_cantons_towns_temp_df["CAN"] == "2B14",
            ["batiments_avec_climatisation", "batiments_sans_climatisation"],
        ].dropna()

        # Check if there are any rows
        if not values_2B14.empty:
            # Get the first row values
            values_2B14 = values_2B14.iloc[0]

            # Fill the missing values in 'batiments_avec_climatisation' and 'batiments_sans_climatisation' for '2B14' with the obtained values
            corsica_buildings_ac_cantons_towns_df.loc[
                (corsica_buildings_ac_cantons_towns_df["CAN"] == "2B14")
                & (corsica_buildings_ac_cantons_towns_df["batiments_avec_climatisation"].isnull()),
                "batiments_avec_climatisation",
            ] = values_2B14["batiments_avec_climatisation"]
            corsica_buildings_ac_cantons_towns_df.loc[
                (corsica_buildings_ac_cantons_towns_df["CAN"] == "2B14")
                & (corsica_buildings_ac_cantons_towns_df["batiments_sans_climatisation"].isnull()),
                "batiments_sans_climatisation",
            ] = values_2B14["batiments_sans_climatisation"]

    final_df = compute_total_buildings(
        corsica_buildings_ac_cantons_towns_df,
        "total_batiments",
        ["batiments_avec_climatisation", "batiments_sans_climatisation"],
    )
    final_df = compute_buildings_with_ac_percent(
        final_df,
        "batiments_avec_climatisation_pourcentage",
        "batiments_avec_climatisation",
        "total_batiments",
    )

    final_df.to_csv("./data/intermediate/air_conditioning/corsica.csv", index=False)
    return final_df


if __name__ == "__main__":
    add_air_conditioning_for_corsica()
