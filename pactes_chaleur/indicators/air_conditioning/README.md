# Analyse de données sur la climatisation dans les bâtiments en France

Cette analyse de données a été réalisée à partir de la base de données nationale des bâtiments (BDNB), disponible sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/base-de-donnees-nationale-des-batiments/). Cette base de données combine plusieurs autres bases de données pour fournir des informations détaillées sur tous les bâtiments en France.

Les données ont été regroupées par commune pour obtenir le nombre total de bâtiments avec et sans climatisation par commune, ainsi que le taux de présence de climatisation par commune.

## 1. Sources de données

Dans le dossier `data/intermediate/air_conditioning/`, on trouve plusieurs données intermédiaires, obtenues à partir de requêtes SQL et d'autres sources de données. 

**`residential_buildings_2022.csv`** 

*Fichier regroupant les informations sur les bâtiments.*

Nous avons téléchargé les données "BDNB - Export france - csv". 
Trois tables de cette base de données ont été utilisées :

1. `Batiment_groupe` : Cette table comprend l'ID du bâtiment, le code du département, le code de la commune, et le code de l'IRIS.

2. `Batiment_groupe_log_type` : Cette table contient également l'ID du bâtiment, une variable booléenne indiquant la présence de climatisation, ainsi que le type de logement (appartement, maison, non résidentiel, logement collectif).

3. `Batiment_groupe_bd_topo` : Cette table comprend également l'ID du bâtiment et le type d'usage du bâtiment (principal, secondaire, agricole, annexe, commerce, etc.).

Les données ont été sélectionnées en croisant les informations de ces trois tables. Les critères de sélection étaient les bâtiments de type maison, appartement et logement collectif dont l'usage principal est résidentiel.

- Requête SQL :

```sql
SELECT 
  bgdrlt.code_departement_insee, 
  bgdrlt.presence_climatisation, 
  bgdrlt.type_batiment, 
  bgr.code_commune_insee, 
  bgr.libelle_commune_insee, 
  bto.l_usage_1, 
  bto.l_nature, 
  bto.l_usage_2 
FROM 
  work_db.public.bat_grp_reduit AS bgr 
  INNER JOIN work_db.public.batiment_groupe_dpe_logtype bgdrlt ON bgr.batiment_groupe_id = bgdrlt.batiment_groupe_id 
  INNER JOIN work_db.public.batiment_groupe_bdtopo_bat bto ON bgr.batiment_groupe_id = bto.batiment_groupe_id 
WHERE 
  type_batiment in (
    'Maison', 'Appartement', 'Logements collectifs'
  ) 
  AND l_usage_1 LIKE '%Résidentiel%';
```

`paris.csv`, `lyon.csv`, `marseille.csv`

*Fichiers sur le nombre de bâtiments sans et avec climatisation dans les arrondissements de Paris, Lyon et Marseille.*

Pour traiter ces communes avec arrondissements, nous avons réalisé une autre manipulation, à savoir :
1. Télécharger les données des bâtiments du département de la commune sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/base-de-donnees-nationale-des-batiments/). Ce sont les fichiers "BDNB - Export dep<numero_dept>", en remplaçant numero_dept par 69 pour Lyon par exemple.
2. Extraire les mêmes informations que pour `residential_buildings_2022.csv`, en gardant également l'iris et la commune. Voici la requête SQL lancée pour chacune des trois communes :

```sql
SELECT 
  bgdrlt.code_departement_insee, 
  bgdrlt.presence_climatisation, 
  bgr.code_iris, 
  bgdrlt.type_batiment, 
  bgr.code_commune_insee, 
  bgr.libelle_commune_insee, 
  bto.l_usage_1, 
  bto.l_nature, 
  bto.l_usage_2 
FROM 
  work_db.public.bat_grp_reduit as bgr 
  INNER JOIN work_db.public.batiment_groupe_dpe_logtype bgdrlt ON bgr.batiment_groupe_id = bgdrlt.batiment_groupe_id 
  INNER JOIN work_db.public.batiment_groupe_bdtopo_bat bto on bgr.batiment_groupe_id = bto.batiment_groupe_id 
WHERE 
  type_batiment in (
    'Maison', 'Appartement', 'Logements collectifs'
  ) 
  AND l_usage_1 LIKE '%Résidentiel%';
```

3. En utilisant la table de correspondace IRIS - commune de l'INSEE ([Découpage infracommunal - Table IRIS en géographie au 1er janvier 2022](https://www.insee.fr/fr/information/2017499)), nous avons pu déduire à partir des iris les données par arrondissement.

`comm_corsica_2022.csv`

*Fichier des communes et des cantons de la Corse.*

Ce fichier est un filtre sur la Corse des données sur les cantons qu'on peut retrouver sur le site de l'INSEE ([Code officiel géographique au 1er janvier 2022 - Liste des communes](https://www.insee.fr/fr/information/6051727))

Nous traitons séparemment les communes de la Corse et hors Corse pour des raisons de traitement de chaînes de caractères dans les codes communes de la Corse. 

## 2. Gestion des données manquantes

Après cette sélection, le nombre de communes résultant ne dépasse pas 32 000. Il reste donc environ 4000 communes pour lesquelles les données sont manquantes.

Pour résoudre ce problème, une méthode d'imputation basée sur l'appartenance des communes à un canton spécifique a été utilisée. Les données manquantes ont été remplacées par la moyenne du canton correspondant. Par exemple, pour chaque canton, la moyenne du taux de présence de climatisation a été calculée, puis utilisée pour remplacer les données manquantes des bâtiments appartenant au même canton.

Grâce à cette méthode, un fichier de données complet contenant les données par commune a finalement été obtenu. 