"""
compute_air_conditioning.py
Main script to get the proportion of buildings with air conditioning 
for each metropolitan French town of the 2022 INSEE referential.
"""

import pandas as pd

from pactes_chaleur.indicators.air_conditioning.preprocessing.without_corsica import (
    add_air_conditioning_without_corsica,
)
from pactes_chaleur.indicators.air_conditioning.preprocessing.corsica import (
    add_air_conditioning_for_corsica,
)


def clean_df(full_df: pd.DataFrame) -> pd.DataFrame:
    """Remove unnecessary columns and convert columns to keep to correct type."""

    ordered_columns = [
        "code_commune_insee",
        "libelle_commune_insee",
        "total_batiments",
        "batiments_sans_climatisation",
        "batiments_avec_climatisation",
        "batiments_avec_climatisation_pourcentage",
    ]
    return (
        full_df.astype(
            {
                "code_commune_insee": str,
                "total_batiments": int,
                "batiments_avec_climatisation": int,
                "batiments_sans_climatisation": int,
                "batiments_avec_climatisation_pourcentage": float,
            }
        )
        .assign(
            Libelle=lambda df: df["Libelle"].fillna(df["LIBELLE"]),
            code_commune_insee=lambda df: df["code_commune_insee"].str.zfill(5),
        )
        .drop(
            columns=[
                "Dep",
                "DEP",
                "LIBELLE",
                "moy_batiments_sans_climatisation",
                "moy_batiments_avec_climatisation",
                "temperature_moyenne_ete_C",
                "CAN",
            ]
        )
        .rename(columns={"Libelle": "libelle_commune_insee"})[ordered_columns]
    )


def add_air_conditioning():
    """Main function to process air conditioning data."""

    without_corsica_df = add_air_conditioning_without_corsica()
    corsica_df = add_air_conditioning_for_corsica()
    full_df = pd.concat([without_corsica_df, corsica_df], ignore_index=True)
    final_df = clean_df(full_df)
    final_df.to_csv(
        "./data/processed/comm_arr_hors_dom_2022_batiments_avec_climatisation_pourcentage.csv",
        index=False,
    )


if __name__ == "__main__":
    add_air_conditioning()
