# Préparation des données de couverture végétale

## 1. Récupération des données

Deux jeux de données sont nécessaires pour le calcul du taux de couverture végétale : 
- La base de données Corine Land Cover (disponible sur le site du gouvernement)
- La base de données Georef qui recense les communes de France Métropolitaine (en veillant à ne pas prendre en compte les départements d’outre-mer).

## 2. Pré-traitement des données

Les données CLC doivent être préparées avant traitement afin de minimiser la taille de la couche et par conséquent le temps de traitement. Pour ce faire, il est nécessaire de sélectionner les classes à garder : 

```
-  141. Espaces verts urbains 
-  222. Vergers et petits fruits
-  223. Oliveraies
-  231. Prairies et autres surfaces toujours en herbe à usage agricole
-  244. Territoires agroforestiers
-  311. Forêts de feuillus
-  312. Forêts de conifères 
-  313. Forêts mélangées
-  321. Pelouses et pâturages naturels
-  322. Landes et broussailles
-  323. Végétation sclérophylle
-  324. Forêt et végétation arbustive en mutation
-  333. Végétation clairsemée
```

Pour réaliser cette opération, il faut d’abord catégoriser la couche selon le champ “code_18”, puis ne sélectionner que les numéros de milieux correspondants à la liste ci-dessus. Une fois la catégorisation réalisée, effectuer une simple sélection avec un rectangle en englobant la France métropolitaine (ne pas oublier la Corse).

## 3. Traitement des données

Pour le traitement des données, afin de faciliter le traitement par le logiciel SIG QGis, il est préférable de commencer par 2 étapes de simplifications : 

1. Les données CLC ayant un code à 3 chiffres sont à simplifier en un seul entier. Pour cette étape, nous regroupons les codes par le premier des trois chiffres (ex : 112 devient 1, 213 devient 2, etc.). Un nouveau champ dans la table attributaire est à créer. Nous avons choisi le nom “Niveau 1”. Il prendra l’expression “to_int ( left ”Code_18” , 1 ))”. 
2. La seconde étape de simplification de traitement est un “regrouper”. Elle permet de regrouper ensemble les catégories CLC par “Niveau 1” et de limiter le temps de calcul de l’algorithme utilisé ci-après. 

Finalement nous pouvons effectuer le traitement “intersection”, pour pouvoir superposer les couches et les faire travailler ensemble. Cette étape permet alors d’obtenir une couche complète comportant le croisement des deux couches d'intérêt et de calculer la surface de chaque “Niveau 1” par commune. Il faut créer, dans la table attributaire de notre nouvelle couche, un champ “Surf class” qui prendra l’expression “$area/10000”.

*NB* : En sortie, sur ce fichier, nous obtenons plusieurs valeurs pour une seule commune puisqu’à une commune peut correspondre plusieurs catégories de couverture végétale. Il est donc nécessaire de rassembler ces valeurs pour obtenir une valeur par commune. 

## 4. Calculs de statistiques

Une fois que l’on a les surfaces de milieu végétal par commune, il faut utiliser l'outil “Statistiques par catégories”, en utilisant le champ du code commune. Un fichier sous forme de table attributaire sera disponible, avec un certain nombre de métriques statistiques. Ici, nous nous sommes intéressés à la somme (en effet, la somme permet d’obtenir pour chaque commune la somme de toutes les surfaces de milieu végétal).


