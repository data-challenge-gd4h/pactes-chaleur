"""
coordinates.py
Script to find (latitude, longitude) of each French town.
"""

import pandas as pd
import geopy
from geopy import geocoders
from tqdm import tqdm

tqdm.pandas()


def return_lat_long(
    latitude: float, longitude: float, result_type="tuple"
) -> tuple[float, float] | dict[str, float] | None:
    if result_type == "tuple":
        return latitude, longitude
    elif result_type == "dict":
        return {"latitude": latitude, "longitude": longitude}


def get_town_name(
    address: str, split_char: str, geolocator_name: str, full_libelle_size: int
) -> str | None:
    """Get the French town name from an address given in string.
    The function only accepts three geocoders: "ign" for IGNFrance, "ban" for BANFrance, and "nominatim" for Nominatim.
    Each returns a specific format of address:
        - IGNFrance format: [...], postal_code town_name
            - Example: "rte des forges, 56120 Lanouée" -> "Lanouée"
        - BANFrance format: [...] postal_code town_name
            - Example: "Rue Monseigneur Grandin 53160 Vimartin-sur-Orthe" -> "Vimartin-sur-Orthe"
        - Nominatim format: town_name, [...], postal_code, [...]
            - Example: "Valserhône, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01200, France" -> "Valserhône"

    Args:
        address (str): the location's address from geocoders.geocode
        split_char (str): the characters used to split the address
        geolocator_name (str): the chosen geolocator, between "ign", "ban" and "nominatim"
        full_libelle_size (int): the libelle size when splitted, e.g. "Forges de Lanouée" = 3

    Returns:
        str: French commune name if exists, else None
    """
    address_split = address.split(split_char)
    if geolocator_name == "ign":
        dept_town = address_split[-1]  # e.g. "56120 Lanouée"
        dept_town_split = dept_town.split(" ")
        if len(dept_town_split) >= full_libelle_size + 1:
            return " ".join(dept_town_split[-full_libelle_size:])
    if geolocator_name == "ban":
        if len(address_split) >= full_libelle_size:
            return " ".join(address_split[-full_libelle_size:])
        return address_split[-1]
    if geolocator_name == "nominatim":
        return address_split[0]
    return


def get_department_num(address: str, split_char: str, geolocator_name: str) -> str | None:
    """Get the department's number from an address given in string.
    The function only accepts three geocoders: "ign" for IGNFrance, "ban" for BANFrance, and "nominatim" for Nominatim.
    Each returns a specific format of address:
        - IGNFrance format: [...], postal_code town_name
            - Example: "rte des forges, 56120 Lanouée" -> "56"
        - BANFrance format: [...] postal_code town_name
            - Example: "Rue Monseigneur Grandin 53160 Vimartin-sur-Orthe" -> "53"
        - Nominatim format: town_name, [...], postal_code, [...]
            - Example: "Valserhône, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01200, France" -> "01"

    Args:
        address (str): the location's address from geocoders.geocode
        split_char (str): the characters used to split the address
        geolocator_name (str): the chosen geolocator, between "ign", "ban" and "nominatim"

    Returns:
        str: department's number if exists, else None
    """
    address_split = address.split(split_char)
    if geolocator_name == "ign":
        dept_town = address_split[-1]  # e.g. "56120 Lanouée"
        dept_town_split = dept_town.split(" ")
        return dept_town_split[0][:2]
    if geolocator_name == "ban" or geolocator_name == "nominatim":
        for elem in address_split:
            if elem.isdigit():
                return elem[:2]
    return


def get_lat_long_from_one_geolocator(
    full_libelle: str,
    short_libelle: str,
    num_departement: str,
    geolocator_name: str,
    geolocator: geopy.geocoders,
    split_char: str,
    result_type="tuple",
) -> tuple[float, float] | dict[str, float] | None:
    """Get the latitude and longitude of the French town given by its name (full_libelle) and department's number.

    Args:
        full_libelle (str): French town libelle from INSEE, e.g. "Vimartin-sur-Orthe" or "Paris 1er Arrondissement"
        short_libelle (str): French town libelle without Arrondissement, e.g. "Paris 1er Arrondissement" -> "Paris"
        num_departement (str): the French town's department's number, e.g. "75" for Paris
        geolocator_name (str): the chosen geolocator, between "ign", "ban" and "nominatim"
        geolocator (geopy.geocoders): the geopy.geocoders object, between "IGNFrance()", "BANFrance()", "Nominatim()"
        split_char (str): the characters used to split the address
        result_type (str, optional): either "dict" or "tuple". Defaults to "tuple".

    Returns:
        tuple[float, float] | dict[str, float] | None: latitude, longitude if exists
    """
    try:
        # Search all locations matching the city name
        locations = geolocator.geocode(short_libelle, timeout=3, exactly_one=False)
    except:
        # TimeoutError
        return return_lat_long(None, None, result_type)
    else:
        # Only keep the location matching the department num if exists in location.address
        if locations:
            for location in locations:
                full_libelle_size = len(full_libelle.split(" "))
                location_town_name = get_town_name(
                    location.address, split_char, geolocator_name, full_libelle_size
                )
                location_department_num = get_department_num(
                    location.address, split_char, geolocator_name
                )
                if (
                    location_town_name == full_libelle
                    and location_department_num == num_departement
                ) or (location_town_name == full_libelle and not location_department_num):
                    latitude = round(location.latitude, 2)
                    longitude = round(location.longitude, 2)
                    return return_lat_long(latitude, longitude, result_type)
            return return_lat_long(None, None, result_type)


def get_lat_long(
    libelle_commune_insee: str,
    num_departement: str,
    geolocators: dict,
    result_type="tuple",
) -> tuple[float, float] | dict[str, float] | None:
    """Get the latitude and longitude of the French town given by its name (full_libelle) and department's number.

    Args:
        libelle_commune_insee (str): French town libelle from INSEE, e.g. "Vimartin-sur-Orthe" or "Paris 1er Arrondissement"
        num_departement (str): the French town's department's number, e.g. "75" for Paris
        geolocators (dict): dict of all geolocation services used (IGNFrance, BANFrance, Nominatim),
                            with the characters to split the location's address

    Returns:
        tuple[float, float] | dict[str, float] | None: latitude, longitude if exists
    """

    # Remove Arrondissement when looking for the commune
    if "Arrondissement" in libelle_commune_insee:
        libelle = libelle_commune_insee.split(" ")[0]
    else:
        libelle = libelle_commune_insee

    for geolocator_name, (geolocator, split_char) in geolocators.items():
        lat, long = get_lat_long_from_one_geolocator(
            libelle_commune_insee, libelle, num_departement, geolocator_name, geolocator, split_char
        )
        if lat and long:
            return return_lat_long(lat, long, result_type)
    return return_lat_long(None, None, result_type)


def add_coordinates(source_file: str, dest_file: str):
    """Find the exact coordinates of each metroplitan French town, based on three different geolocators,
    the town's name and department."""

    geolocators = {
        "ign": (geocoders.IGNFrance(), ", "),
        "ban": (geocoders.BANFrance(), " "),
        "nominatim": (geocoders.Nominatim(user_agent="PACTES_chaleur"), ", "),
    }
    towns_df = pd.read_csv(source_file)
    towns_df[["latitude", "longitude"]] = towns_df.progress_apply(
        lambda row: get_lat_long(
            row["libelle_commune_insee"],
            row["num_departement"],
            geolocators,
            result_type="dict",
        ),
        axis="columns",
        result_type="expand",
    )
    towns_df = towns_df.astype({"code_commune_insee": str, "num_departement": str}).assign(
        code_commune_insee=lambda df: df["code_commune_insee"].str.zfill(5),
        num_departement=lambda df: df["num_departement"].str.zfill(2),
    )
    towns_df.to_csv(dest_file, index=False)


if __name__ == "__main__":
    intermediate_dir = "./data/intermediate/"
    add_coordinates(
        intermediate_dir + "comm_arr_hors_dom_dept_2022.csv",
        intermediate_dir + "comm_arr_hors_dom_dept_lat_long_2022.csv",
    )
