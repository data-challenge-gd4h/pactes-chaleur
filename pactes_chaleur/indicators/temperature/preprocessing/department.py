"""
department.py
Script to add department's number for each French town.

The department's number is the first two number of the postal code, 
not the INSEE department's code.
This is needed to get the latitude and longitude of a French town.
"""

import pandas as pd


def add_department(source_file: str, dest_file: str):
    """Extract the department's number from INSEE's code."""

    cols = ["num_departement", "code_commune_insee", "libelle_commune_insee"]
    towns_df = pd.read_csv(source_file)

    # Add department code and reorder column
    towns_df = (
        towns_df.assign(num_departement=lambda df: df["code_commune_insee"].str[:2])
        .replace(
            {"num_departement": "2A"},
            {"num_departement": "20"},
        )
        .replace(
            {"num_departement": "2B"},
            {"num_departement": "20"},
        )[cols]
    )

    # Save
    towns_df.to_csv(dest_file, index=False)


if __name__ == "__main__":
    intermediate_dir = "./data/intermediate/"
    add_department(
        intermediate_dir + "comm_arr_hors_dom_2022.csv",
        intermediate_dir + "comm_arr_hors_dom_dept_2022.csv",
    )
