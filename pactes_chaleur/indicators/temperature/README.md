# Préparation des données de température

## 1. Source de données

Nous avons utilisé les données de Copernicus, à partir de l'[API Historical Weather](https://open-meteo.com/en/docs/historical-weather-api), qui permet de requêter les températures quotidiennes moyennes pour des coordonnées géographiques et une plage de date données.

## 2. Traitement des données

1. Extraction des départements de chaque commune à partir des codes INSEE ([`preprocessing/department.py`](./preprocessing/department.py)).

En effet pour trouver les bonnes coordonnées géographiques de chaque commune, le libellé INSEE ne suffit pas, il nous faut aussi leur numéro de département (pas le code département de l’INSEE) :
- Pour la majorité, ce numéro correspond aux deux premiers chiffres du code commune INSEE
- Pour les communes de la Corse, on remplace “2A” et “2B” par “20”
- Il y a juste une commune dont le numéro ne correspond pas aux deux premiers chiffres du code commune INSEE, mais cela ne pose pas de problème dans la recherche.


2. Extraction des coordonnées géographiques de chaque commune à partir du nom de la commune et de son département ([`preprocessing/coordinates.py`](./preprocessing/coordinates.py)).

- Pour les geocoders, on utilise tout d’abord IGNFrance, puis BANFrance si on ne trouve rien avec le premier puis Nominatim (OpenStreetMap) si on ne trouve rien avec les deux premiers.
- Pour les villes avec arrondissements (Paris, Lyon, Marseille), on cherche par le nom de la ville sans arrondissement et on regarde dans la liste des locations qu’on obtient, celle qui correspond à la ville avec l’arrondissement.
- Difficultés rencontrées :
  - Les différents formats des adresses que donne les geocoders en sortie, pour avoir le nom de la commune et le numéro département. Nous avons écrit un code personnalisé pour chaque cas.
  - Il existe des communes de départements différents (e.g. Héricourt dans le 62, et 70), donc on les différencie par le numéro du département.
  - Pour les villes avec arrondissements, on les trouve que si on enlève “… Arrondissement” dans le libellé de la commune (e.g. "Paris 1er Arrondissement" → "Paris"). Ensuite pour bien récupérer la latitude, longitude de l’arrondissement, on fait un matching du libellé complet avec le libellé qu’on trouve avec geocoder.
  - Parfois dans l’adresse de geocoder, il n’y a pas le département, donc on regarde seulement le matching avec le libellé

3. Requête pour récupérer des moyennes de températures quotidiennes et calcul de la moyenne estivale ([`compute_temperature.py`](./compute_temperature.py))

## 3. Limite de la méthode

Cette méthode fonctionne dans le cadre du challenge, mais n'est pas scalable car on réalise une requête pour chaque commune, et on traite une grande quantité de données.
Pour donner un ordre de grandeur, nous avions 34 868 communes métropolitaines au total, 30 + 31 + 30 de données quotidiennes à récupérer pour chacune d'entre elle, donc au total 3 172 988 à traiter. Cela a pris 5h en continu (sans erreur et arrêt de notre propre machine), et beaucoup plus longtemps en pratique.

Une solution de stockage des données et un accès à un autre fournisseur de données méteo (ex : Météo France) permettrait de pérenniser le traitement des données de température.
