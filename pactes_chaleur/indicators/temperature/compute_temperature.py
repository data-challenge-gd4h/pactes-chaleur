"""
compute_temperature.py
Main script to get the 2022 average summer temperature 
for each metropolitan French town of the 2022 INSEE referential.
"""

import os
import requests
import json
import csv
import pandas as pd
from statistics import mean
from datetime import datetime
from natsort import natsorted

HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
    "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "cross-site",
    "Sec-Fetch-User": "?1",
}
INPUT_FILE = "./data/intermediate/comm_arr_hors_dom_dept_lat_long_2022.csv"
OUTPUT_FILE = "./data/processed/comm_arr_hors_dom_2022_temperature_moyenne_ete_C.csv"


def add_temperature_by_chunk(result_filepath: str, towns_chunk_df: pd.DataFrame):
    """For a number of towns listed in `towns_chunk_df`,
    get the 2022 summer daily mean temperature for each town,
    and write the average summer temperature in a csv file given by `result_filepath`
    """

    with open(result_filepath, "a", newline="") as file:
        writer = csv.writer(file)
        for _, row in towns_chunk_df.iterrows():
            libelle = row["libelle_commune_insee"]
            code = row["code_commune_insee"]
            print(libelle, code)
            latitude, longitude = row["latitude"], row["longitude"]
            request_url = f"https://archive-api.open-meteo.com/v1/archive?latitude={latitude}&longitude={longitude}&start_date=2022-06-01&end_date=2022-08-31&daily=temperature_2m_mean&timezone=Europe/London"
            try:
                response = requests.get(request_url, headers=HEADERS, timeout=3)
            except:
                print("==> TimeoutError")
                writer.writerow([libelle, code, latitude, longitude, None])
            else:
                data_json = json.loads(response.text)
                avg_summer_temperature_C = round(mean(data_json["daily"]["temperature_2m_mean"]), 2)
                writer.writerow([libelle, code, latitude, longitude, avg_summer_temperature_C])


def compute_town_coverage(df: pd.DataFrame):
    """Compute the number of towns with and without temperature data."""

    num_towns = len(df)
    num_nan = df["temperature_moyenne_ete_C"].isna().sum()
    num_temp = num_towns - num_nan
    percent_nan = round((num_nan * 100) / num_towns, 2)
    percent_temp = 100 - percent_nan
    print(f"Nombre de communes avec T°C : {num_temp}/{num_towns} ({percent_temp}%)")
    print(f"Nombre de communes sans T°C : {num_nan}/{num_towns} ({percent_nan}%)")


################################################################
# ################## FUNCTIONS FOR 1ST PASS ####################
################################################################


def add_temperature(start: int = None, end: int = None):
    """Get the average 2022 summer temperature of each French town.
    between n° start end n° end included. We procede chunk by chunk, of size 100.

    Example:
        If start = 1, end = 5000, we compute the temperature of the towns of 1 to 5000 included.
        In other words, the first 5000 towns.
        The "1-5000.csv" file is created in "data/intermediate/temperature", with the following columns:
            - "libelle_commune_insee",
            - "code_commune_insee",
            - "latitude",
            - "longitude",
            - "temperature_moyenne_ete_C".

    Args:
        start (int, optional): the starting commune's n°. Defaults to None.
        end (int, optional): the ending commune's n° (included). Defaults to None.
    """
    towns_df = pd.read_csv(INPUT_FILE)

    # Compute average summer temperature chunk by chunk of size chunk_size
    chunk_size = 100
    num_towns = len(towns_df)
    i_start = 0 if not start else start - 1  # To get the index in dataframe of start, it is -1
    i_end = num_towns if not end else end

    result_filepath = f"./data/intermediate/temperature/{start}-{end}.csv"
    if not os.path.exists(result_filepath):
        with open(result_filepath, "w", newline="") as file:
            writer = csv.writer(file)
            writer.writerow(
                [
                    "code_commune_insee",
                    "libelle_commune_insee",
                    "latitude",
                    "longitude",
                    "temperature_moyenne_ete_C",
                ]
            )

    for i in range(i_start, i_end, chunk_size):
        if (i + chunk_size) < i_end:
            current_slice = slice(i, i + chunk_size)
        else:
            current_slice = slice(i, i_end)
        print(
            f"===> {current_slice.start}-{current_slice.stop}: {datetime.now().strftime('%H:%M:%S')}"
        )
        towns_chunk_df = towns_df.iloc[current_slice]
        add_temperature_by_chunk(result_filepath, towns_chunk_df)


def merge_temperature():
    """Concatenation of csv files created with "add_temperature()" in a final csv file"""

    intermediate_dir = "./data/intermediate/temperature/"
    list_files = [f for f in os.listdir(intermediate_dir)]
    list_files = natsorted(list_files)
    list_df = [pd.read_csv(intermediate_dir + filename) for filename in list_files]
    df = pd.concat(list_df)
    df.to_csv(OUTPUT_FILE, index=False)


################################################################
# ################## FUNCTIONS FOR 2ND PASS ####################
################################################################
# After the 1st pass, missing T°C because TimeoutError
# We just need to run again


def add_missing_temperature(df: pd.DataFrame):
    """Get the average 2022 summer temperature of each French town
    where temperature data was not computed in the first pass."""

    mask_nan = df["temperature_moyenne_ete_C"].isna()
    isna_df = df[mask_nan].copy()
    result_filepath = "./data/intermediate/temperature/missing_temperature.csv"
    with open(result_filepath, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(
            [
                "code_commune_insee",
                "libelle_commune_insee",
                "latitude",
                "longitude",
                "temperature_moyenne_ete_C",
            ]
        )
    add_temperature_by_chunk(result_filepath, isna_df)


def merge_missing_temperature(incomplete_df, with_missing_values_df):
    """Merge the retrieved temperature data in the 2nd pass to the 1st pass."""

    def get_index(df, code):
        return df.index[df["code_commune_insee"] == code].values[0]

    # To keep the order of INSEE file
    # We cannot order by code_commune_insee because for Corse, the code begins with "2A" or "2B"
    complete_df = incomplete_df.copy()
    for _, row in with_missing_values_df.iterrows():
        code = row["code_commune_insee"]
        temperature = row["temperature_moyenne_ete_C"]
        idx = get_index(complete_df, str(code))
        complete_df.at[idx, "temperature_moyenne_ete_C"] = temperature

    # Save
    complete_df.to_csv(OUTPUT_FILE, index=False)


def add_temperature(first_pass=False, second_pass=False):
    """Main function to process temperature data.
    We process chunk by chunk of 5000 towns, and in two passes."""

    if first_pass:
        # Compute temperature, and save file 5000 by 5000
        start = 1
        end = 34997
        chunk_size = 5000
        for i in range(start, end, chunk_size):
            if (i + chunk_size) < end:
                add_temperature(i, i + chunk_size - 1)
            else:
                add_temperature(i, end)

        # Merge all files
        merge_temperature()

    if second_pass:
        incomplete_df = pd.read_csv(OUTPUT_FILE)
        with_missing_values_df = pd.read_csv(
            "./data/intermediate/temperature/missing_temperature.csv"
        )
        merge_missing_temperature(incomplete_df, with_missing_values_df)

    # Final commune coverage
    df = pd.read_csv(OUTPUT_FILE)
    compute_town_coverage(df)


if __name__ == "__main__":
    add_temperature()
