"""
compute_all_indicators.py
Main script to get all the indicators
for each metropolitan French town of the 2022 INSEE referential.

The script is calling each data process function (starts with "add_<indicator>()".
Since we have already computed all indicators and saved in different csv file, 
the script is by default reading the files, computing z-scores and merging them.
"""

import sys
import pandas as pd
import scipy.stats as stats

from pactes_chaleur.indicators.deprivation_index.compute_deprivation_index import (
    add_deprivation_index,
)
from pactes_chaleur.indicators.air_conditioning.compute_air_conditioning import add_air_conditioning
from pactes_chaleur.indicators.temperature.compute_temperature import add_temperature


def add_all_indicators(from_scratch: bool = False):
    """Compute all indicators and z-scores for each.
    Save in comm_arr_hors_dom_2022_all_indicators.csv.

    Args:
        from_scratch (bool, optional): compute from scratch or not. Defaults to False.
            Since computing temperature takes a lot of time (several hours),
            we advise not to run from scratch.
    """

    if from_scratch:
        add_deprivation_index()
        add_air_conditioning()
        add_temperature(first_pass=True, second_pass=True)

    columns = [
        "fdep15",
        "batiments_avec_climatisation_pourcentage",
        "temperature_moyenne_ete_C",
        "tx_cv",
    ]
    z_scores_dfs = []
    for col in columns:
        filepath = f"./data/processed/comm_arr_hors_dom_2022_{col}.csv"
        df = pd.read_csv(filepath)
        z_scores_dfs.append(
            pd.DataFrame(
                {
                    "code_commune_insee": df["code_commune_insee"],
                    col: df[col],
                    f"z_score_{col}": stats.zscore(df[col]),
                }
            )
        )

    final_df = z_scores_dfs[0]
    for i in range(1, len(z_scores_dfs)):
        final_df = final_df.merge(z_scores_dfs[i], on="code_commune_insee")
    final_df = final_df.merge(
        pd.read_csv("./data/intermediate/comm_arr_hors_dom_2022.csv"), on="code_commune_insee"
    )
    columns_to_keep = (
        ["code_commune_insee", "libelle_commune_insee"]
        + columns
        + [f"z_score_{col}" for col in columns]
    )
    final_df[columns_to_keep].to_csv(
        "./data/processed/comm_arr_hors_dom_2022_all_indicators.csv", index=False
    )


if __name__ == "__main__":
    if len(sys.argv) == 1:
        add_all_indicators()
    elif sys.argv[1] == "from_scratch":
        add_all_indicators(from_scratch=True)
    else:
        print("\nDefault usage:\n\tpython pactes_chaleur/indicators/compute_all_indicators.py\n")
        print(
            "To run from scratch:\n\tpython pactes_chaleur/indicators/compute_all_indicators.py from_scratch\nSince computing temperature takes a lot of time (several hours), we advise not to run from scratch."
        )
