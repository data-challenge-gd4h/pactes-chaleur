"""
compute_deprivation_index.py
Main script to get the INSERM's deprivation index (FDep) 
for each metropolitan French town of the 2022 INSEE referential.
"""

import pandas as pd
import numpy as np


def compute_nearest_town_index_mean(towns_fdep_df: pd.DataFrame) -> pd.DataFrame:
    """Compute the deprivation index for towns with missing index,
    based on nearest towns' deprivation index mean."""

    # Calculation of the average of the fdep15 index of the communes closest to Sannerville
    towns_fdep_df.loc[towns_fdep_df["libelle_commune_insee"] == "Sannerville", "fdep15"] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Touffréville")
            | (towns_fdep_df["code_commune_insee"] == "14215")
            | (towns_fdep_df["libelle_commune_insee"] == "Démouville")
            | (towns_fdep_df["libelle_commune_insee"] == "Émiéville")
            | (towns_fdep_df["libelle_commune_insee"] == "Escoville")
            | (towns_fdep_df["libelle_commune_insee"] == "Saint-Pair")
            | (towns_fdep_df["libelle_commune_insee"] == "Troarn")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Les Trois Lacs
    towns_fdep_df.loc[towns_fdep_df["libelle_commune_insee"] == "Les Trois Lacs", "fdep15"] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Villers-sur-le-Roule")
            | (towns_fdep_df["libelle_commune_insee"] == "Muids")
            | (towns_fdep_df["libelle_commune_insee"] == "Bouafles")
            | (towns_fdep_df["libelle_commune_insee"] == "Courcelles-sur-Seine")
            | (towns_fdep_df["libelle_commune_insee"] == "Vézillon")
            | (towns_fdep_df["libelle_commune_insee"] == "La Roquette")
            | (towns_fdep_df["libelle_commune_insee"] == "Le Thuit")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Trébons-de-Luchon
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Trébons-de-Luchon", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Benque-Dessous-et-Dessus")
            | (towns_fdep_df["libelle_commune_insee"] == "Saint-Aventin")
            | (towns_fdep_df["libelle_commune_insee"] == "Saccourvielle")
            | (towns_fdep_df["libelle_commune_insee"] == "Castillon-de-Larboust")
            | (towns_fdep_df["libelle_commune_insee"] == "Bagnères-de-Luchon")
            | (towns_fdep_df["libelle_commune_insee"] == "Moustajon")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Beaumont-en-Verdunois
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Beaumont-en-Verdunois", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Ville-devant-Chaumont")
            | (towns_fdep_df["libelle_commune_insee"] == "Moirey-Flabas-Crépion")
            | (towns_fdep_df["libelle_commune_insee"] == "Ornes")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Bezonvaux
    towns_fdep_df.loc[towns_fdep_df["libelle_commune_insee"] == "Bezonvaux", "fdep15"] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Dieppe-sous-Douaumont")
            | (towns_fdep_df["libelle_commune_insee"] == "Douaumont-Vaux")
            | (towns_fdep_df["libelle_commune_insee"] == "Ornes")
            | (towns_fdep_df["libelle_commune_insee"] == "Maucourt-sur-Orne")
            | (towns_fdep_df["libelle_commune_insee"] == "Mogeville")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Cumières-le-Mort-Homme
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Cumières-le-Mort-Homme", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Chattancourt")
            | (towns_fdep_df["libelle_commune_insee"] == "Marre")
            | (towns_fdep_df["libelle_commune_insee"] == "Champneuville")
            | (towns_fdep_df["libelle_commune_insee"] == "Regnéville-sur-Meuse")
            | (towns_fdep_df["libelle_commune_insee"] == "Béthincourt")
            | (towns_fdep_df["libelle_commune_insee"] == "Forges-sur-Meuse")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Fleury-devant-Douaumont
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Fleury-devant-Douaumont", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Douaumont-Vaux")
            | (towns_fdep_df["libelle_commune_insee"] == "Eix")
            | (towns_fdep_df["libelle_commune_insee"] == "Moulainville")
            | (towns_fdep_df["libelle_commune_insee"] == "Damloup")
            | (towns_fdep_df["libelle_commune_insee"] == "Belleville-sur-Meuse")
            | (towns_fdep_df["libelle_commune_insee"] == "Bras-sur-Meuse")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Haumont-près-Samogneux
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Haumont-près-Samogneux", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Samogneux")
            | (towns_fdep_df["libelle_commune_insee"] == "Brabant-sur-Meuse")
            | (towns_fdep_df["libelle_commune_insee"] == "Moirey-Flabas-Crépion")
            | (towns_fdep_df["libelle_commune_insee"] == "Consenvoye")
        ]["fdep15"].mean(),
        6,
    )

    # Calculation of the average of the fdep15 index of the communes closest to Louvemont-Côte-du-Poivre
    towns_fdep_df.loc[
        towns_fdep_df["libelle_commune_insee"] == "Louvemont-Côte-du-Poivre", "fdep15"
    ] = round(
        towns_fdep_df[
            (towns_fdep_df["libelle_commune_insee"] == "Douaumont-Vaux")
            | (towns_fdep_df["libelle_commune_insee"] == "Vacherauville")
            | (towns_fdep_df["libelle_commune_insee"] == "Bras-sur-Meuse")
            | (towns_fdep_df["libelle_commune_insee"] == "Ornes")
        ]["fdep15"].mean(),
        6,
    )

    return towns_fdep_df


def add_deprivation_index():
    """Main function to process deprivation index data."""

    towns_df = pd.read_csv("./data/intermediate/comm_arr_hors_dom_2022.csv")
    fdep_df = pd.read_csv("./data/raw/deprivation_index/INSERM_2015/FDep_2015.txt", sep=";")
    fdep_df["fdep15"] = fdep_df["fdep15"].str.replace(",", ".").astype(float)
    towns_fdep_df = pd.merge(
        fdep_df, towns_df, left_on=["CODGEO"], right_on=["code_commune_insee"], how="right"
    ).assign(is_equal=lambda df: np.where(df["CODGEO"] == df["code_commune_insee"], 1, 0))[
        ["CODGEO", "code_commune_insee", "libelle_commune_insee", "P15_POP", "fdep15", "Q5"]
    ]
    towns_fdep_df = compute_nearest_town_index_mean(towns_fdep_df)
    towns_fdep_df[["code_commune_insee", "libelle_commune_insee", "fdep15"]].to_csv(
        "./data/processed/comm_arr_hors_dom_2022_fdep15.csv", index=False
    )


if __name__ == "__main__":
    add_deprivation_index()
