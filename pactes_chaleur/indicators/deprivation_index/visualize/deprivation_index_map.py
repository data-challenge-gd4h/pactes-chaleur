import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt


def create_deprivation_index_map():
    """Function to visualize the deprivation index on a map of France."""

    # Load data
    towns_df = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_fdep15.csv")
    towns_gdf = gpd.read_file("./data/raw/communes-20220101-shp/communes-20220101.shp")

    # Merge
    merge_df = pd.merge(
        left=towns_gdf, right=towns_df, left_on="nom", right_on="libelle_commune_insee", how="left"
    )

    # Create the map
    _, ax = plt.subplots(figsize=(8, 8))

    # Plot the choropleth map
    merge_df.plot(column="fdep15", cmap="YlGn", legend=True, ax=ax)

    # Add a title
    ax.set_title("Deprivation index of metropolitan French towns (2022)")

    # Set the x and y limits to zoom in on the map
    ax.set_xlim([-5, 10])
    ax.set_ylim([40, 55])

    # Save the map
    plt.savefig("./figures/deprivation_index_map.png")
    plt.show()


if __name__ == "__main__":
    create_deprivation_index_map()
