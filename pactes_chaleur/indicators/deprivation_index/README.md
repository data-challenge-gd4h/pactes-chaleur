# Préparation des données d'indice de défavorisation

## 1. Récupération des données 

Le jeu de données est disponible sur le site [cepidc.inserm.fr](https://www.cepidc.inserm.fr/documentation/indicateurs-ecologiques-du-niveau-socio-economique)

La base de données contient l'indice de défavorisation (FDep) de 2015 pour la France métropolitaine. 
Les limites géographiques communales utilisées sont datées au 1er janvier 2017. Le fichier comporte 35 322 lignes et 4 variables.

Les variables contiennent les informations suivantes : 
- colonne CODGEO : Code commune INSEE
- colonne P15_POP : population communale au 1er janvier 2015
- colonne fdep15 : plus le score est élevé, plus le désavantage social est important
- colonne Q5 : classification de l'indice en quintiles (5 catégories) allant des moins désavantagées (Q5=1) au plus désavantagées (Q5=5)

Les variables utilisées pour la suite de l'étude sont : CODGEO et fdep15.

## 2. Traitements des données 
### 2.1. Fusion des données avec le fichier AtlaSanté

Les données ont été fusionnées avec la liste des communes d'AtlaSanté datant de 2022 (hors DOM). Suite à cette fusion, nous constatons 9 données manquantes du score fdep15 pour 9 communes. 

### 2.2. Traitement des données manquantes

Pour résoudre ce problème, nous avons imputé les données manquantes par la moyenne du score de défavorisation des communes les plus proches de la commune manquante. 

Ainsi, nous avons obtenu un fichier de données complet comprenant un score de défavorisation pour l'ensemble des communes de la France métropolitaine. 





