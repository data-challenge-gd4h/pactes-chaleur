import pandas as pd


def add_atlasante():
    """Merge heat score and morbidity data for each French metropolitan town.
    The file is then sent to Atlasante in order to integrate the data into their map"""

    heat_score_df = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_indic_vuln.csv", sep=",")
    morbidity_df = pd.read_csv("./data/intermediate/morbidity/morbidity.csv", sep=",")
    atlasante_df = heat_score_df.merge(morbidity_df, on="code_commune_insee", how="left")[
        [
            "code_commune_insee",
            "libelle_commune_insee",
            "indic_vuln",
            "nb_iCanicule",
            "nb_hospits_iCanicule",
        ]
    ]
    atlasante_df.to_csv("./data/processed/PACTES_chaleur_atlasante.csv", index=False)


if __name__ == "__main__":
    add_atlasante()
