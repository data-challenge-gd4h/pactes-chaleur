"""
compute_heat_score.py
Main script to get the heat score for each metropolitan French town of the 2022 INSEE referential.
"""

import pandas as pd


def add_heat_score():
    """Compute heat score as the sum of all indicators' z-scores,
    and save in file."""

    heat_score_df = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_all_indicators.csv")
    heat_score_df["indic_vuln"] = (
        heat_score_df["z_score_fdep15"]
        + heat_score_df["z_score_temperature_moyenne_ete_C"]
        + heat_score_df["z_score_batiments_avec_climatisation_pourcentage"]
        + heat_score_df["z_score_tx_cv"]
    )
    heat_score_df[["code_commune_insee", "libelle_commune_insee", "indic_vuln"]].to_csv(
        "./data/processed/comm_arr_hors_dom_2022_indic_vuln.csv", index=False
    )


if __name__ == "__main__":
    add_heat_score()
