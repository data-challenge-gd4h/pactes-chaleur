import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def create_heat_score_map():
    """Function to visualize the heat score on a map of France."""

    # Load data
    heat_score_df = pd.read_csv("./data/processed/comm_arr_hors_dom_2022_indic_vuln.csv")
    communes_gdf = gpd.read_file("./data/raw/communes-20220101-shp/communes-20220101.shp")

    # Merge
    merge_df = pd.merge(
        left=communes_gdf,
        right=heat_score_df,
        left_on="nom",
        right_on="libelle_commune_insee",
        how="left",
    )

    # Prepare the colors
    keys = list(merge_df["indic_vuln_intervalle"].unique())
    color_range = list(np.linspace(0, 1, len(keys), endpoint=False))
    colors = [cm.tab20b(x) for x in color_range]
    color_dict = dict(zip(keys, colors))

    # Prepare the grid
    row_count = merge_df["indic_vuln_intervalle"].nunique()
    ax_list = []
    for i in range(row_count + 1):
        ax_list.append("ax" + str(i + 1))
        ax_string = ", ".join(ax_list)

    # Create the map
    fig, (ax_string) = plt.subplots(row_count, 4)

    # Plot the map
    ax1 = plt.subplot2grid((row_count, 4), (0, 1), rowspan=row_count, colspan=3)
    for _, row in merge_df.iterrows():
        plot = merge_df[merge_df["indic_vuln"] == row["indic_vuln"]].plot(
            color=color_dict[row["indic_vuln_intervalle"]], ax=ax1
        )
        ax1.axis("off")

    # Plot the legend
    row_counter = 0
    for i in merge_df["indic_vuln_intervalle"].unique():
        plt.subplot2grid((row_count, 4), (row_counter, 0))
        plt.pie([1], labels=[i], radius=0.4, colors=[color_dict[i]])
        plt.axis("off")
        row_counter += 1

    # Save the map
    plt.savefig("./figures/heat_score_map.png")
    plt.show()


if __name__ == "__main__":
    create_heat_score_map()
