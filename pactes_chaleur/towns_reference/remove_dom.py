"""
remove_dom.py
Remove all French DOM (INSEE code starting with 97).
"""

import re
import pandas as pd


def read_excel(filepath: str, range: str):
    """Returns an excel sheet as a dataframe.

    Args:
        filepath (str): excel file path
        range (str): cells' range to read, e.g. "C5:G8"

    Returns:
        pd.DataFrame: excel sheet
    """

    cells = range.split(":")
    cols_rows = [re.split(r"(\d+)", s) for s in cells]
    first_col = cols_rows[0][0]
    first_row = int(cols_rows[0][1])
    last_col = cols_rows[1][0]
    last_row = int(cols_rows[1][1])
    usecols = first_col + ":" + last_col
    skiprows = first_row - 1
    nrows = last_row - first_row + 1

    df = pd.read_excel(
        filepath,
        usecols=usecols,
        skiprows=skiprows,
        nrows=nrows,
        engine="openpyxl",
    )
    return df


def remove_dom(source_file: str, dest_file: str, source_range: str):
    """Remove DOM from the INSEE 2022 towns referential."""

    df_communes = read_excel(source_file, range=source_range)
    df_communes = df_communes.drop(
        df_communes[df_communes["Code"].astype(str).str.startswith("97")].index
    ).rename(columns={"Libellé": "libelle_commune_insee", "Code": "code_commune_insee"})
    df_communes.to_csv(dest_file, index=False)


if __name__ == "__main__":
    source_file = "./data/raw/comm_arr_2022.xlsx"
    dest_file = "./data/intermediate/comm_arr_hors_dom_2022.csv"
    source_range = "A3:B35000"
    remove_dom(source_file, dest_file, source_range)
