# Contributing Guidelines

Thank you for your interest in contributing to our project! We welcome all contributions, including bug reports, feature requests, documentation improvements, and code changes.

To ensure that your contributions can be accepted quickly and easily, please follow these guidelines.

## Naming Conventions

- `<>` : replace by the name of your folder, file etc.
- `[]` : optional, if it exists

| Type | Convention | General Example | Example |
| ---- | ---------- | --------------- | ------- |
| folder, file | In lower case, separated by `_` | `<name>` | `temperature_data` |
| variable | In lower case, separated by `_` | `description>_[<unit>]_<variable_type>` | `temperature_C_df`|
| function | In lower case, separated by `_` | `<action>_<description>()` | `get_dates()` |
| class | In camel case | `class <DescriptionOfTheClass>` | `class LandCover`

## Reporting Issues
If you find a bug or have a suggestion for a feature, you can open a new [issue](https://gitlab.com/data-challenge-gd4h/pactes-chaleur/-/issues/new).

When opening an issue:

> 1. Check the [issues](https://gitlab.com/data-challenge-gd4h/pactes-chaleur/-/issues) and [pull requests](https://gitlab.com/data-challenge-gd4h/pactes-chaleur/-/merge_requests) to make sure that the feature/bug has not already been addressed, or is being worked upon.
> 2. Provide as much detail as possible about the issue, including steps to reproduce the issue if applicable.

## Submitting Pull Requests
We welcome contributions to this repository.  
  
If you want to contribute a feature or fix a bug, please follow these steps:

> 1. Check the [issues](https://gitlab.com/data-challenge-gd4h/pactes-chaleur/-/issues) and [pull requests](https://gitlab.com/data-challenge-gd4h/pactes-chaleur/-/merge_requests) to make sure that the feature/bug has not already been addressed, or is being worked upon.
> 2. Fork the repository and create a new branch for your changes.
> 3. Make the changes in your fork.
> 4. ***Test your changes*** thoroughly to make sure they are working as expected.
> 5. ***Add documentation*** for your changes, if applicable.
> 6. Open a pull request to this repository.
> 7. In the pull request description, explain the changes you have made and why they are necessary.

## Code Style
Please follow the [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) in the project when making code changes.

When using VSCode, you can format your code change on save. Check the steps [here](https://dev.to/adamlombard/how-to-use-the-black-python-code-formatter-in-vscode-3lo0) to configure in VSCode. In the settings, go to "Python > Formatting: Black Args", and add the following argument `--line-length`, then `100`.

## Code Review
All code changes must be reviewed and approved by at least one member of the project team before they can be merged into the main branch. Code reviews may include comments or requests for changes, which should be addressed before the changes can be merged.

Thank you for your interest in contributing to our project!