# Installation Guide

- [Data Collection](#data-collection)
- [Dependencies](#dependencies)
- [Development](#development)
- [Production](#production)

## Data Collection

The data necessary for this project are listed in [data/README.md](./data/README.md). For each data, a link and the time period of interest are provided.

The [data](./data/) is organized as the following:
- [raw](./data/raw): all raw files downloaded directly from the source links. There are not modified.
- [intermediate](./data/intermediate): raw files that are pre-processed and needed to obtain the final data.
- [processed](./data/processed): final files, one for each indicator, one with all indicators and a file for AtlaSanté. The files and their respective script are listed in the [README.md](./data/README.md). 

## Dependencies

- **Python version**: 3.10.11
- **Libraries and packages**: listed in [`pyproject.toml`](./pyproject.toml)
- **Virtual environment**: [`pyenv`](https://github.com/pyenv/pyenv)
- **Dependency manager**: [`poetry`](https://python-poetry.org/) 

### Setup virtual environment

#### 1. Prerequisites
Install the [prerequisites](/PREREQUISITES.md) if you have not already done so.

#### 2. Clone this repository 

1. Fork the repository.
2. In a command prompt of your choice, clone your fork.
3. Go in the project folder.

#### 3. Setting up the environment

Doing the following step will enable your local environement to be aligned with the one of any other collaborator.

__3.1 Create a virtual environment__

In a command prompt (use Git bash in Windows):
1. Check existing Python versions with `pyenv`
    ```bash
    pyenv versions
    ```
2. If Python 3.10.11 does not exist, install it
    ```bash
    pyenv install 3.10.11
    ```
3. Set Python 3.10.11 for the local project
    ```bash
    pyenv local 3.10.11
    ```
4. Using `virtualenv`, create a virtual environment named ".env_pactes_chaleur"
    ```bash
    python -m venv .env_pactes_chaleur
    ```
    
5. Activate the virtual environment
    ```bash
    source .env_pactes_chaleur/bin/activate # zsh, bash, etc.
    . .env_pactes_chaleur/Scripts/activate # Git bash
    .\.env_pactes_chaleur\Scripts\activate # Windows PowerShell
    ```

__3.2 Install packages with `poetry`__

Now we need a tool to manage dependencies. Let's use `poetry`.

1. Install `poetry`
    ```bash
    pip install poetry
    ```

2. Install all dependencies
    ```bash
    poetry install
    ```

When you need to install a new package (e.g. nltk), run
```bash
poetry add nltk
```
:warning: Then __commit the files `poetry.lock` and `pyproject.toml`__ to ensure that the package versions are consistent for everyone working on the project.

## Development

To run the solution locally, let's describe how to navigate in the project:

1. [towns_reference](/pactes_chaleur/towns_reference/) contains the script for keeping only French metropolitan towns, excluding towns in the French overseas departments and territories. This is used to generate the [towns reference for the project](/data/intermediate/comm_arr_hors_dom_2022.csv).
2. [indicators](/pactes_chaleur/indicators/) contains scripts for processing data for each indicator and each French metropolitan town.
It also contains the script for processing [morbidity](/pactes_chaleur/indicators/morbidity/) data. The script [compute_all_indicators.py](/pactes_chaleur/indicators/compute_all_indicators.py) launches the processing of each indicator and calculates their z-scores.

    - Default usage: since the data have already been processed and saved in their specific csv files, the following command reads the files and calculates the z-scores:
        ```
        python pacts_chaleur/indicators/compute_all_indicators.py
        ```
    - To start processing temperature, air-conditioning and social deprivation index data:
        ```
        python pactes_chaleur/indicators/compute_all_indicators.py from_scratch
        ```
         However, we recommend that you do not run this command, as temperature data processing takes a very long time (+5h), as explained [here](./pactes_chaleur/indicators/temperature/README.md).


3. [heat_score](/pactes_chaleur/heat_score/) contains the script to calculate the vulnerability index. 

        python pactes_chaleur/heat_score/compute_heat_score.py
        
4. [atlasante](/pactes_chaleur/atlasante/) contains the script used to generate a data file containing the vulnerability index and morbidity data for each French metropolitan town. This file is used by AtlaSanté to integrate it into Sirsé for mapping purposes.

## Production

There is no production mode for this project at the moment.
