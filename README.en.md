# Challenge GD4H - Heat Pacts
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

The <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

---
**Table of contents:**
- [Heat PACTES](#heat-pactes)
- [Directory structure](#directory-structure)
- [Documentation](#documentation)
    - [Installation](#installation)
    - [Use](#usage)
    - [Contributing](#contributing)
    - [Licence](#licence)
- [Team](#team)

## Heat PACTES

A healthcare professional setting up practice is not familiar with the environmental and health determinants in their area, and therefore cannot anticipate the risks for their patients.

<a href="https://gd4h.ecologie.gouv.fr/defis/795" target="_blank" rel="noreferrer">Find out more about the challenge</a>

## **Directory structure**

```
- data/ ----------------------------- raw and final data for AtlaSanté
        README.md ------------------- data sources and the INSEE communes repository (2022)
        intermediate/ --------------- intermediate files created from raw data
        processed/ ------------------ final files for AtlaSanté
        raw/ ------------------------ raw data
- figures/ -------------------------- temperature maps and deprivation index
- pactes_chaleur/ ------------------- source code 
        atlasante/ ------------------ script for the vulnerability and morbidity indicator 
        heat_score/ ----------------- script z score for each indicator and vulnerability index
        indicators/ ----------------- scripts for each indicator
        towns_reference/ ------------ script to create the reference system to be used for the project (metropolitan France INSEE 2022)
- tests/ ---------------------------- test files
- .gitignore ------------------------ files and directories that git ignores
- .python-version ------------------- version of python 
- CONTRIBUTING.md ------------------- guidelines for contributions
- INSTALL.md ------------------------ installation guide for dependencies etc.
- PREREQUISITES.md ------------------ prerequisites to install before INSTALL.md
- README.en.md ---------------------- README of the project (English, current file)
- README.md ------------------------- README of the project 
- licence.MIT + licence.etalab-2.0 -- licence
- poetry.lock + pyproject.toml ------ poetry files to install dependencies
```

## **Documentation**

PACTES Chaleur is a mapping tool that will be made available on AtlaSanté's Sirsé platform. Its aim is to identify the populations most vulnerable to heat waves at local level in mainland France. 

A population vulnerability index has been constructed. 

This index comprises 4 indicators: 

- average summer temperature in 2022
- level of air-conditioning in homes
- vegetation cover in the municipality
- social deprivation index

To create this index, we first processed each of the data separately, then calculated the z-score for each indicator by municipality. The vulnerability index per municipality is simply the sum of the z-scores if they exist for these 4 indicators, otherwise it is zero.

Morbidity indicators (number of emergency room visits and hospitalisations) linked to heat waves are also proposed.

### **Installation**

[Installation guide](/INSTALL.md)

### **Usage**

***Navigating to the data folder***

1. [raw](/data/raw/): raw data for each indicator and commune code data for mainland France and DOM TOM (overseas departments and territories). 
2. [intermediate](/data/intermediate/): intermediate data
3. [processed](/data/processed/): final data

The complete README can be found in [data/README.md](./data/README.md).

***Browsing the heat pacts folder

To navigate the [pactes_chaleur](/pactes_chaleur/) folder, proceed as follows:

1. [towns_reference](/pactes_chaleur/towns_reference/) contains the script used to keep only towns in mainland France, excluding towns in French overseas departments and territories. This is used to generate the [reference for the project](/data/intermediate/comm_arr_hors_dom_2022.csv).
2. [indicators](/pactes_chaleur/indicators/) contains the scripts for processing data for the 4 indicators for all communes in mainland France excluding DOM TOM. 
It also contains the script for processing [morbidity] data (/pactes_chaleur/indicators/morbidity/). The script [compute_all_indicators.py](/pactes_chaleur/indicators/compute_all_indicators.py) is used to process each indicator and calculate their z-scores.

    - Default usage: as the data has already been processed and saved in its specific csv files, the following command is used to read the files and calculate the z-scores:
        ```
        python pactes_chaleur/indicators/compute_all_indicators.py
        ```
    - To start processing the temperature, air conditioning and social deprivation index data:
        ```
        python pactes_chaleur/indicators/compute_all_indicators.py from_scratch
        ```
        However, we recommend that you do not run this command, as the temperature data takes a very long time to process (5h+), as explained [here](./pactes_chaleur/indicators/temperature/README.md).


3. [heat_score](/pactes_chaleur/heat_score/) contains the script used to calculate the vulnerability index. 

        python pacts_heat/heat_score/compute_heat_score.py
        
4. [atlasante](/pactes_chaleur/atlasante/) contains the script used to generate a data file containing the vulnerability index and morbidity data for each commune in mainland France. This file is used by AtlaSanté to integrate it into Sirsé to create a map.


### **Contributing**

If you would like to contribute to this project, please follow the [guidelines](/CONTRIBUTING.md).

### **License**

The code is released under the [MIT](/licence.MIT) licence.

The data referenced in this README and in the installation guide are published under the [Etalab Open Licence 2.0](/licence.etalab-2.0).


## **Team**

### Project leaders

- Dr. François Carbonnel
- Dr. Grégoire Mercier
- Paul Bassobert
- Karolina Griffiths
- Mirelle Abraham

### Volunteers

- Elise Chin
- Emilie Agrech
- Camille Debrock
- Layana Caroupaye-Caroupin
- Manal Ahikki
- Imen Kadri
