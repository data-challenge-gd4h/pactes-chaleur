# Challenge GD4H - PACTES chaleur
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

---
**Table des matières :**
- [PACTES chaleur](#pactes-chaleur)
- [Structure du répertoire](#structure-du-répertoire)
- [Documentation](#documentation)
    - [Installation](#installation)
    - [Utilisation](#utilisation)
    - [Contributions](#contributions)
    - [Licence](#licence)
- [Equipe](#equipe)

## PACTES chaleur

Un professionnel de santé qui s’installe ne connaît pas les déterminants environnementaux et de santé de son territoire, et ne peut donc pas anticiper les risques de ses patients.

<a href="https://gd4h.ecologie.gouv.fr/defis/795" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Structure du répertoire**

```
- data/ ----------------------------- données brutes et finales pour AtlaSanté
        README.md ------------------- sources de données et le référentiel des communes INSEE (2022)
        intermediate/ --------------- fichiers intermédiaires créés à partir des données brutes
        processed/ ------------------ fichiers finaux pour AtlaSanté
        raw/ ------------------------ données brutes
- figures/ -------------------------- cartes des températures et indice de défavorisation
- pactes_chaleur/ ------------------- code source 
        atlasante/ ------------------ script indicateur de vulnérabilité et morbidité 
        heat_score/ ----------------- script z score de chaque indicateur et indice de vulnérabilité
        indicators/ ----------------- scripts pour chaque indicateur
        towns_reference/ ------------ script pour créer le référentiel à utiliser pour le projet (France métropolitaine INSEE 2022)
- tests/ ---------------------------- fichiers tests
- .gitignore ------------------------ fichiers et répertoires que git ignore
- .python-version ------------------- version de python 
- CONTRIBUTING.md ------------------- lignes directrices pour les contributions
- INSTALL.md ------------------------ guide d'installation des dépendances etc.
- PREREQUISITES.md ------------------ prérequis à installer avant INSTALL.md
- README.en.md ---------------------- README du projet (anglais)
- README.md ------------------------- README du projet (fichier actuel)
- licence.MIT + licence.etalab-2.0 -- licence
- poetry.lock + pyproject.toml ------ fichiers poetry pour installer les dépendences
```

## **Documentation**

PACTES Chaleur est un outil cartographique qui sera mis à disposition sur la plateforme Sirsé d'AtlaSanté. Il vise à identifier les populations les plus vulnérables aux vagues de chaleur à l'échelle communale de la France Métropolitaine. 

Un indice de vulnérabilité des populations  a été construit. 

Cet indice regroupe 4 indicateurs : 

- température moyenne estivale en 2022
- taux d'équipement des logements en climatisation
- couverture végétale de la commune
- indice de défavorisation sociale

Afin de créer cet indice, nous avons tout d’abord traité chacune des données séparément, puis calculer le z-score de chaque indicateur par commune. L'indice de vulnérabilité par commune est simplement la somme des z-scores s’il existe pour ces 4 indicateurs, sinon il est nul.

Des indicateurs de morbidité (nombre de passages aux urgences et le nombre d'hospitalisation) liés aux vagues de chaleurs sont également proposés.

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

***Navigation dossier data***

1. [raw](/data/raw/) : données brutes de chaque indicateur et données des codes communes de la France Métropolitaine et DOM TOM 
2. [intermediate](/data/intermediate/) : données intermédiaires
3. [processed](/data/processed/) : données finales

Le README complet se trouve dans [data/README.md](./data/README.md).

***Navigation dossier pactes chaleur***

Afin de naviguer dans le dossier [pactes_chaleur](/pactes_chaleur/), il faut procéder de la manière suivante :

1. [towns_reference](/pactes_chaleur/towns_reference/) contient le script permettant de garder uniquement les communes de la France métropolitaine en excluant les communes des DOM TOM. Celui-ci permet la génération du [référentiel pour le projet](/data/intermediate/comm_arr_hors_dom_2022.csv).
2. [indicators](/pactes_chaleur/indicators/) contient les scripts de traitement des données des 4 indicateurs pour l'ensemble des communes de la France métropolitaine hors DOM TOM. 
Elle contient également le script permettant de traiter les données de [morbidités](/pactes_chaleur/indicators/morbidity/). Le script [compute_all_indicators.py](/pactes_chaleur/indicators/compute_all_indicators.py) permet de lancer le traitement de chaque indicateur et de calculer leurs z-scores.

    - Utilisation par défaut : les données étant déjà traitées, et enregistrées dans leurs fichiers csv spécifiques, la commande suivante permet de lire les fichiers et de calculer les z-scores :
        ```
        python pactes_chaleur/indicators/compute_all_indicators.py
        ```
    - Pour lancer les traitements des données de température, de climatisation et de l'indice de défavorisation sociale :
        ```
        python pactes_chaleur/indicators/compute_all_indicators.py from_scratch
        ```
        Nous recommandons cependant de ne pas lancer cette commande car le traitement des données de température est très longue (+5h) comme expliqué [ici](./pactes_chaleur/indicators/temperature/README.md).


3. [heat_score](/pactes_chaleur/heat_score/) contient le script permettant de calculer l'indice de vulnérabilité. 

        python pactes_chaleur/heat_score/compute_heat_score.py
        
4. [atlasante](/pactes_chaleur/atlasante/) contient le script permettant de générer un fichier de données contenant l'indice de vulnérabilité et les données de morbidités pour chaque commune de la France métropolitaine. Ce fichier est utilisé par AtlaSanté afin de l'intégrer au sein de Sirsé pour créer une cartographie.


### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).


## **Equipe**

### Porteurs du projet

- Dr. François Carbonnel
- Dr. Grégoire Mercier
- Paul Bassobert
- Karolina Griffiths
- Mirelle Abraham

### Bénévoles

- Elise Chin
- Emilie Agrech
- Camille Debrock
- Layana Caroupaye-Caroupin
- Manal Ahikki
- Imen Kadri
