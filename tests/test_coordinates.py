import unittest
from geopy import geocoders

from pactes_chaleur.indicators.temperature.preprocessing.coordinates import (
    get_town_name,
    get_department_num,
    get_lat_long,
)


class LocationTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.geolocators = {
            "ign": (geocoders.IGNFrance(), ", "),
            "ban": (geocoders.BANFrance(), " "),
            "nominatim": (geocoders.Nominatim(user_agent="PACTES_chaleur"), ", "),
        }
        return super().setUp()

    def test_get_town_name(self):
        # IGNFrance format: [...], postal_code commune_name
        address1 = "58420 Champallement"
        address2 = "rte des forges, 56120 Lanouée"

        # BANFrance format: [...] postal_code commune_name
        address3 = "Rue Monseigneur Grandin 53160 Vimartin-sur-Orthe"
        address4 = "Vimartin-sur-Orthe"
        address5 = "Guillérien 56120 Forges de Lanouée"

        # Nominatim format: commune_name, [...], postal_code, [...]
        address6 = "Valserhône, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01200, France"
        address7 = "Bâgé-Dommartin, Bourg-en-Bresse, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01380, France"
        address8 = "Douaumont-Vaux, Verdun, Meuse, Grand Est, France métropolitaine, France"

        comm1 = get_town_name(address1, ", ", "ign", 1)
        comm2 = get_town_name(address2, ", ", "ign", 3)
        comm3 = get_town_name(address3, " ", "ban", 1)
        comm4 = get_town_name(address4, " ", "ban", 1)
        comm5 = get_town_name(address5, " ", "ban", 3)
        comm6 = get_town_name(address6, ", ", "nominatim", 1)
        comm7 = get_town_name(address7, ", ", "nominatim", 1)
        comm8 = get_town_name(address8, ", ", "nominatim", 1)

        assert comm1 == "Champallement"
        assert comm2 == None
        assert comm3 == "Vimartin-sur-Orthe"
        assert comm4 == "Vimartin-sur-Orthe"
        assert comm5 == "Forges de Lanouée"
        assert comm6 == "Valserhône"
        assert comm7 == "Bâgé-Dommartin"
        assert comm8 == "Douaumont-Vaux"

    def test_get_department_num(self):
        # IGNFrance format: [...], postal_code commune_name
        address1 = "58420 Champallement"
        address2 = "rte des forges, 56120 Lanouée"

        # BANFrance format: [...] postal_code commune_name
        address3 = "Rue Monseigneur Grandin 53160 Vimartin-sur-Orthe"
        address4 = "Vimartin-sur-Orthe"

        # Nominatim format: commune_name, [...], postal_code, [...]
        address5 = "Valserhône, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01200, France"
        address6 = "Bâgé-Dommartin, Bourg-en-Bresse, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01380, France"
        address7 = "Douaumont-Vaux, Verdun, Meuse, Grand Est, France métropolitaine, France"

        dept_num1 = get_department_num(address1, ", ", "ign")
        dept_num2 = get_department_num(address2, ", ", "ign")
        dept_num3 = get_department_num(address3, " ", "ban")
        dept_num4 = get_department_num(address4, " ", "ban")
        dept_num5 = get_department_num(address5, ", ", "nominatim")
        dept_num6 = get_department_num(address6, ", ", "nominatim")
        dept_num7 = get_department_num(address7, ", ", "nominatim")

        assert dept_num1 == "58"
        assert dept_num2 == "56"
        assert dept_num3 == "53"
        assert dept_num4 == None
        assert dept_num5 == "01"
        assert dept_num6 == "01"
        assert dept_num7 == None

    def test_get_lat_long(self):
        # Unique city
        assert get_lat_long("Champallement", "58", self.geolocators) == (47.23, 3.49)

        # Cities' names duplicates, but different department
        assert get_lat_long("Héricourt", "62", self.geolocators) == (50.35, 2.26)
        assert get_lat_long("Héricourt", "70", self.geolocators) == (47.58, 6.76)

        # City with different arrondissement (Paris, Marseille, Lyon; 46 in total)
        assert get_lat_long("Paris 1er Arrondissement", "75", self.geolocators) == (48.86, 2.34)
        assert get_lat_long("Paris 20e Arrondissement", "75", self.geolocators) == (48.87, 2.4)
        assert get_lat_long("Marseille 8e Arrondissement", "13", self.geolocators) == (43.27, 5.38)
        assert get_lat_long("Lyon 4e Arrondissement", "69", self.geolocators) == (45.77, 4.83)

        # City that can be found with Nominatim, and not IGN France
        assert get_lat_long("Bâgé-Dommartin", "01", self.geolocators) == (46.33, 4.97)
        assert get_lat_long("Valserhône", "01", self.geolocators) == (46.13, 5.81)

        # Error in city and department
        assert get_lat_long("Prs", "=0", self.geolocators) == (None, None)

        # Error in city only
        assert get_lat_long("Prs", "93", self.geolocators) == (None, None)

        # Error in department only -> return the coord of the city, regarless of department num
        assert get_lat_long("Paris", "93", self.geolocators) == (48.86, 2.35)

        # Corse (2A, 2B)
        assert get_lat_long("Ajaccio", "20", self.geolocators) == (41.92, 8.74)
        assert get_lat_long("Ficaja", "20", self.geolocators) == (42.42, 9.37)

        # No department num in address
        assert get_lat_long("Douaumont-Vaux", "55", self.geolocators) == (49.21, 5.47)

        # len(city.split(" ")) > 1
        assert get_lat_long("Forges de Lanouée", "56", self.geolocators) == (48.01, -2.58)


if __name__ == "__main__":
    unittest.main()
