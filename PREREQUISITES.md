# Prerequisites

## Index

- [Python](#install-python)
- [Visual Studio Code (VSCode)](#install-visual-studio-code-vscode)
- [Gitlab](#create-a-gitlab-account)
- [Git](#git)
- [`pyenv` and `virtualenv`](#pyenv-and-virtualenv)
    - [Ubuntu](#ubuntu)
    - [Windows](#windows)
- [Code Style](#code-style)

---
## **Install Python**
[Download the latest version of Python](https://www.python.org/downloads/)

## **Install Visual Studio Code (VSCode)**
Install a development environment, e.g. [Visual Studio Code (VSCode)](https://code.visualstudio.com/download)

## **Create a Gitlab account**

Gitlan is an online platform that allows, among other things, to host Git repositories.

Create a [Gitlab](https://about.gitlab.com/) account with your email address. If you have a GitHub account, you can also use it to sign in.

**SSH key**

With an SSH key, you can connect to Gitlab from your machine without providing your username and personal access token on every visit. You can also use an SSH key to sign commits. So we will add a new SSH key to our GitHub account, to link our local machine to the account.

Add an SSH key to your Gitlab account:
- [Check for existing SSH key](https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair)
- If you don't have an existing key, [generate one](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)
- [Add a new SSH key](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)

## **Git**

Git is the most widely used version control system. It allows you to keep a history of changes and versions of each file in a project.

1. Install [Git](https://git-scm.com/download/win)
2. Accept all default choices
3. Validate the installation by taping `git` in the command prompt of your choice
4. In the same terminal, configure your identity. **Use the same email address as your Gitlab account.**
    ```bash
    git config --global user.name "John Doe"
    git config --global user.email "johndoe@example.com"
    ```


For a step-by-step Git and GitHub tutorial, go to this [link](https://elxse.notion.site/Git-et-Github-57337f89f6ae4ca9b6097507e88a1030). 


## **`pyenv` and `virtualenv`**

Doing the following step will enable your local environement to be aligned with the one of any other collaborator.

### **Ubuntu**

**Install `pyenv`**

[`pyenv`](https://github.com/pyenv/pyenv) is a simple python version management tool. It allows us to install and run different versions of Python on the same machine. 

1. Install all required prerquisite dependencies
    ```bash
    sudo apt-get update; sudo apt-get install make build-essential libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
    libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
    ```
2. Download and execute installation script
    ```bash
    curl https://pyenv.run | bash
    ```
3. Add the following entries into your resource file (`~/.bashrc` is you using bash, `~/.zshrc` if you using zsh)
    ```bash
    # pyenv
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init --path)"
    eval "$(pyenv virtualenv-init -)"
    ```
4. Restart your shell
    ```bash
    exec $SHELL
    ```
5. Validate installation
    ```bash
    pyenv --version
    ```

**Install `virtualenv`**

Using the local Python, install `virtualenv` to manage your virtual environment
```bash
pip install virtualenv
```

### **Windows**

**Install `pyenv`**

[`pyenv`](https://github.com/pyenv/pyenv) is a simple python version management tool. It allows us to install and run different versions of Python on the same machine. 

You can follow the steps below to install on Windows using [`pyenv-win`](https://github.com/pyenv-win/pyenv-win).

1. At the root of your computer, install `pyenv-win` in **Windows PowerShell** using `git clone`
    ```bash
    git clone https://github.com/pyenv-win/pyenv-win.git "$HOME\.pyenv"
    ```
2. Add `PYENV`, `PYENV_HOME` and `PYENV_ROOT` to your Environment Variables
    ```bash
    [System.Environment]::SetEnvironmentVariable('PYENV',$env:USERPROFILE + "\.pyenv\pyenv-win\","User")
    ```

    ```bash
    [System.Environment]::SetEnvironmentVariable('PYENV_ROOT',$env:USERPROFILE + "\.pyenv\pyenv-win\","User")
    ```

    ```bash
    [System.Environment]::SetEnvironmentVariable('PYENV_HOME',$env:USERPROFILE + "\.pyenv\pyenv-win\","User")
    ```

3. Add the following paths to your USER PATH variable in order to access the `pyenv` command
    ```bash
    [System.Environment]::SetEnvironmentVariable('path', $env:USERPROFILE + "\.pyenv\pyenv-win\bin;" + $env:USERPROFILE + "\.pyenv\pyenv-win\shims;" + [System.Environment]::GetEnvironmentVariable('path', "User"),"User")
    ```

**Validate installation of `pyenv`**

1. Reopen the command prompt and run `pyenv --version`
If you are getting any **UnauthorizedAccess** error as below:
    ```
    pyenv : Impossible de charger le fichier C:\Users\$USERNAME\.pyenv\pyenv-win\bin\pyenv.ps1, car l’exécution
    de scripts est désactivée sur ce système. Pour plus d’informations, consultez about_Execution_Policies à l’adresse
    https://go.microsoft.com/fwlink/?LinkID=135170.
    Au caractère Ligne:1 : 1
    + pyenv
    + ~~~~~
        + CategoryInfo          : Erreur de sécurité : (:) [], PSSecurityException
        + FullyQualifiedErrorId : UnauthorizedAccess
    ```
    You need to explicitely allow PowerShell to run scripts. Navigate to the following `Paramètres > Confidentialité et sécurité > Espace développeurs`. Scroll down to the bottom of the page, click on "PowerShell" and check the box "Modifier la stratégie d'exécution...".

2. Type `pyenv` to view it's usage
3. If you are using VSCode's terminal, you need to update VSCode's environment variables in order to access the pyenv command. Go to `File > Preferences > Settings > Terminal›Integrated›Env: Windows`, and click on `Edit in settings.json`. Add the following:
    ```json
        "terminal.integrated.env.windows": {
            "Path": $env:Path,
            "PYENV": $env:PYENV,
            "PYENV_ROOT": $env:PYENV_ROOT,
            "PYENV_HOME": $env:PYENV_HOME
        },
    ```
    Just write `$env:Path` on Windows PowerShell outside VSCode to get the value of the variable.


**Install `virtualenv`**

Using the local Python, install `virtualenv` to manage your virtual environment
```bash
python -m pip install virtualenv
```

## Code Style

Please follow the [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) in the project when making code changes.

When using VSCode, you can format your code change on save. Check the steps [here](https://dev.to/adamlombard/how-to-use-the-black-python-code-formatter-in-vscode-3lo0) to configure in VSCode. In the settings, go to "Python > Formatting: Black Args", and add the following argument `--line-length`, then `100`.